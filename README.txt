To run any of the homework assignments:
1- get eclipse
2- click on file --> import
3- general --> existing project into workspace
4- select archive files
5- browse and select the zipped file with the name of the homework, e.g. percolation
6- compile and run