public class Subset {
	public static void main(String[] args) {

		int k = Integer.parseInt(args[0]);
		int N = 0;
		
		RandomizedQueue<String> randQue = new RandomizedQueue<String>();
		
		while (!StdIn.isEmpty()) {
			N++;
			if (N <= k) {
				randQue.enqueue(StdIn.readString());
			}
			else if (StdRandom.uniform(N)+1 <= k) {
				randQue.dequeue();
				randQue.enqueue(StdIn.readString());
			}
			else {
				StdIn.readString();
			}
		}
		
		for (String s : randQue) {
			StdOut.println(s);
		}
	}
}

