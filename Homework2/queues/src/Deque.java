import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

	private int N;
	private Node first, last;

	private class Node {
		private Item item;
		private Node next, previous;
	}

	public Deque() {
		first = null;
		last = null;
		N = 0;
	}

	public boolean isEmpty() {

		return first == null || last == null;
	}

	public int size() {
		return N;
	}

	public void addFirst(Item item) {
		if (item == null)
			throw new NullPointerException("Null insertion");
		Node oldfirst = first;
		first = new Node();
		first.item = item;
		first.previous = null;
		first.next = oldfirst;
		N++;
		if (isEmpty())
			last = first;
		else
			oldfirst.previous = first;
		// assert check();
	}

	public void addLast(Item item) {
		if (item == null)
			throw new NullPointerException("Null insertion");
		Node oldlast = last;
		last = new Node();
		last.item = item;
		last.next = null;
		last.previous = oldlast;
		N++;
		if (isEmpty())
			first = last;
		else
			oldlast.next = last;

	}

	public Item removeFirst() {
		if (isEmpty())
			throw new NoSuchElementException("Stack underflow");
		Item item = first.item;
		first = first.next;
		N--;
		if (isEmpty())
			last = null;
		else
			first.previous = null;
		return item;
	}

	public Item removeLast() {
		if (isEmpty())
			throw new NoSuchElementException("Stack underflow");
		Item item = last.item;
		last = last.previous;
		N--;
		if (isEmpty())
			first = null;
		else
			last.next = null;
		return item;
	}

	// return an iterator over items in order from front to end
	public Iterator<Item> iterator() {
		return new ListIterator();
	}

	private class ListIterator implements Iterator<Item> {
		private Node current = first;

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext())
				throw new NoSuchElementException();
			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	public static void main(String[] args) {
		Deque<String> s = new Deque<String>();
		s.addFirst("1");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.addLast("2");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.addLast("3");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.addFirst("4");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeLast();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeLast();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeLast();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeFirst();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		// s.removeLast();
		s.addFirst("4");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.addLast("3");
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeFirst();
		// Iterator<String> i = s.iterator();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());

		s.removeLast();
		// Iterator<String> i = s.iterator();
		for (String s1 : s)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + s.size());
	}
}