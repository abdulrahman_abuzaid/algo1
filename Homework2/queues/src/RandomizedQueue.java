import java.util.Iterator;
import java.util.NoSuchElementException;

// import java.;

public class RandomizedQueue<Item> implements Iterable<Item> {
	private Item[] a;
	private int N;

	public RandomizedQueue() {
		a = (Item[]) new Object[1];
	}

	public boolean isEmpty() {
		return N == 0;
	}

	public int size() {
		return N;
	}

	private void resize(int capacity) {
		assert capacity >= N;
		Item[] temp = (Item[]) new Object[capacity];
		for (int i = 0; i < N; i++) {
			temp[i] = a[i];
		}
		a = temp;
	}

	public void enqueue(Item item) {
		if (item == null)
			throw new NullPointerException("Null insertion");
		if (N == a.length)
			resize(2 * a.length);
		a[N++] = item;
	}

	public Item dequeue() {
		if (isEmpty())
			throw new NoSuchElementException("Stack underflow");
		int randomIndex = StdRandom.uniform(N);
		Item item = a[randomIndex];
		a[randomIndex] = a[N - 1];
		a[N - 1] = null;
		N--;
		if (N > 0 && N == a.length / 4)
			resize(a.length / 2);
		return item;
	}

	public Item sample() {
		if (isEmpty())
			throw new NoSuchElementException("Stack underflow");
		return a[StdRandom.uniform(N)];
	}

	public Iterator<Item> iterator() {
		return new ArrayIterator();
	}

	private class ArrayIterator implements Iterator<Item> {
		private int i, randomIndex;
		private Item[] temp = (Item[]) new Object[a.length];
		private Item item;

		public ArrayIterator() {
			i = N;
			for (int j = 0; j < a.length; j++) {
				temp[j] = a[j];
			}
		}

		public boolean hasNext() {
			return i > 0;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext())
				throw new NoSuchElementException();
			randomIndex = StdRandom.uniform(i);
			this.item = temp[randomIndex];
			temp[randomIndex] = temp[--i];
			temp[i] = null;
			return item;
		}
	}

	public static void main(String[] args) {
		RandomizedQueue<String> r = new RandomizedQueue<String>();
		r.enqueue("1");
		r.enqueue("2");

		/*
		 * r.enqueue("3"); r.enqueue("4"); r.enqueue("5"); r.enqueue("6");
		 * r.enqueue("7"); r.enqueue("8"); r.enqueue("9"); r.enqueue("10");
		 */
		for (String s1 : r)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + r.size());
		r.dequeue();
		for (String s1 : r)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + r.size());
		r.dequeue();
		for (String s1 : r)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + r.size());
		r.enqueue("8");
		r.enqueue("9");
		r.enqueue("10");
		for (String s1 : r)
			StdOut.print(s1 + " ");
		StdOut.println("\nsize: " + r.size());
		StdOut.println("\nsample: " + r.sample());
	}
}