public class Percolation
{
    private boolean[][] grid;
    private int pos;
    private WeightedQuickUnionUF uf, uf2;
    private int num;

    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        num = N;
        grid = new boolean[num][num];
        uf = new WeightedQuickUnionUF(num*num+2);
        uf2 = new WeightedQuickUnionUF(num*num+2);
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                grid[i][j] = false;
            }
        }
    }
    private int xyTo1D(int x, int y) {
        return x + num*(y-1);
    }
    private boolean isValid(int x, int y) {
        if (x >= 1 && x <= num && y >= 1 && y <= num) {
            return true;
        }
        return false;
    }
    // open site (row i, column j) if it is not already
    public void open(int i, int j) {
        if (i < 1 || i > num) 
            throw new IndexOutOfBoundsException("row index i out of bounds");
        if (j < 1 || j > num) 
            throw new IndexOutOfBoundsException("column index j out of bounds");
        grid[i-1][j-1] = true;
        pos = xyTo1D(i, j);
        if (isValid(i+1, j) && isOpen(i+1, j)) {
            uf.union(pos, xyTo1D(i+1, j));
            uf2.union(pos, xyTo1D(i+1, j));
        }
        if (isValid(i-1, j) && isOpen(i-1, j)) {
            uf.union(pos, xyTo1D(i-1, j));
            uf2.union(pos, xyTo1D(i-1, j));
        }
        if (isValid(i, j+1) && isOpen(i, j+1)) {
            uf.union(pos, xyTo1D(i, j+1));
            uf2.union(pos, xyTo1D(i, j+1));
        }
        if (isValid(i, j-1) && isOpen(i, j-1)) {
            uf.union(pos, xyTo1D(i, j-1));
            uf2.union(pos, xyTo1D(i, j-1));
        }
        if (i == 1) {
            uf.union(pos, 0);
            uf2.union(pos, 0);
        }
        if (i == num) {
            uf.union(pos, num*num+1);
        }
    }
    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        return grid[i-1][j-1];
    }  
    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        if (i <= 0 || i > num) 
            throw new IndexOutOfBoundsException("row index i out of bounds");
        if (j <= 0 || j > num) 
            throw new IndexOutOfBoundsException("column index j out of bounds");
        return uf.connected(0, xyTo1D(i, j)) && uf2.connected(0, xyTo1D(i, j));
    }
    // does the system percolate?
    public boolean percolates() {
        return uf.connected(0, num*num+1);
    }
}
