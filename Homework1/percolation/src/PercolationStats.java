public class PercolationStats
{
    private int[] arr;
    private int i, j, count, num, times;

    // perform T independent computational experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0)
            throw new IllegalArgumentException("Illegal arguments.");
        num = N;
        times = T;
        arr = new int[times];
        for (int k = 1; k <= times; k++) {
            Percolation perc = new Percolation(num);
            count = 0;
            while (!perc.percolates()) {
                i = (int) (1+num*Math.random());
                j = (int) (1+num*Math.random());
                if (!perc.isOpen(i, j)) {
                    count++;
                    perc.open(i, j);
                }		
            }
            arr[k-1] = count;
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(arr)/(num*num);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(arr)/(num*num);
    }

    // returns lower bound of the 95% confidence interval
    public double confidenceLo() {
        return mean()-1.96*stddev()/Math.sqrt(times);
    }
    // returns upper bound of the 95% confidence interval
    public double confidenceHi() {
        return mean()+1.96*stddev()/Math.sqrt(times);
    }
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats percStats = new PercolationStats(N, T);
        System.out.println("mean                    = "+percStats.mean());
        System.out.println("stddev                  = "+percStats.stddev());
        System.out.print("95% confidence interval = "+percStats.confidenceLo());
        System.out.println(", "+percStats.confidenceHi());
    }
}

